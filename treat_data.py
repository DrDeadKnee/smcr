import configs
from scipy.interpolate import interp1d
from ui import UI_capsule as ask
from matplotlib import pyplot as plt

def visual_inspect(xmat, ymat):
    ##working on this function. need to allow user to group spectra into n classes, where n is
    ##the user's choice
    
    question = "Do you wish to remove any of these spectra?"
    possible_answers = []
   
    maxmat = []
    for i in range(len(ymat)):
        maxval = max(ymat[i])
        maxvec = []
        for j in range(len(ymat[i])):
            maxvec.append(ymat[i][j]/maxval)
        maxmat.append(maxvec)

    for i in range(len(xmat)):
        plt.plot(xmat[i],maxmat[i])
        possible_answers.append(str(i))
    plt.show()
   
    user_answer = ask(question, possible_answers)

    return xmat, ymat


def clip_spectra(xmat, ymat):
    print "Trimming Ends of Spectra"
    
    xmin = configs.get("x_minimum")
    xmax = configs.get("x_maximum")

    kill_mat = []
    

    #check sorting
    forwardsort = True
    backwardsort = True
    for i in range(len(xmat)):
        for j in range(len(xmat[i])-1):
            if forwardsort:
                if xmat[i][j] > xmat[i][j+1]:
                    forwardsort = False
            if backwardsort:
                if xmat[i][j] < xmat[i][j+1]:
                    backwardsort = False


    #apply index multiplier for counting 
    if backwardsort and not forwardsort:
        indexmultiplier = -1
    elif forwardsort and not backwardsort:
        indexmultiplied = 1
    else:
        print "Not sorted! goinfg to crash now!"


    #find regions above and below specified limits
    mindex = 0
    maxdex = len(xmat[0])
    for i in range(len(xmat[0])):
        if xmat[0][i*indexmultiplier] < xmin and i > mindex:
            mindex = i

        if xmat[0][i*indexmultiplier] > xmax and i < maxdex:
            maxdex = i

    

    #construct new arrays using data from desired regions
    clippedxarray = []
    clippedyarray = []
    
    if backwardsort:
        for i in range(len(xmat)):
            clippedxarray.append(xmat[i][-1*maxdex:-1*mindex])
            clippedyarray.append(ymat[i][-1*maxdex:-1*mindex])
    elif forwardsort:
        for i in range(len(xmat)):
            clippedxarray.append(xmat[i][mindex:maxdex])
            clippedyarray.append(ymat[i][mindex:maxdex])



    #output
    return clippedxarray, clippedyarray

 
    
def align_spectra(xmat, ymat):    
    print "Aligning Spectra. Note that this will truncate the dataset by the lowest and higest values."

    #find all spectra with identical x values, and track their indices 
    x_begins = [[1]]
    for i in range(len(xmat)):
        counted = False
        for j in range(len(x_begins)):
            if x_begins[j][0] == xmat[i][0]:
                counted = True
                x_begins[j].append(i)
        if not counted:
            x_begins.append([xmat[i][0],i])


    #determine which set of x indixes to use
    nspecswithx = []
    for i in range(len(x_begins)):
        nspecswithx.append(len(x_begins[i])-1)
    bestx = x_begins[nspecswithx.index(max(nspecswithx))][0]
    bestxlist = xmat[x_begins[nspecswithx.index(max(nspecswithx))][1]][1:-2]
    bestxmat = []
    for i in range(len(xmat)):
        bestxmat.append(bestxlist)
    

    #get an array with nothing but indices marked for correction
    del x_begins[nspecswithx.index(max(nspecswithx))]
    del x_begins[0]
    for i in x_begins:
        del i[0]

    correctdex = []
    for i in range(len(x_begins)):
        for j in range(len(x_begins[i])):
            correctdex.append(x_begins[i][j])


    #interpolate y values for marked spectra with cubic spline
    cs_fits = []
    for i in correctdex:
        cs = interp1d(xmat[i],ymat[i],kind="cubic",bounds_error="false")
        cs_fits.append(cs)
  


    #generate interpolated y values for marked spectra using the best x spectrum.
    ycorrect = []
    for i in range(len(cs_fits)):
        ycorrect.append(cs_fits[i](bestxlist))
    for i in range(len(ymat)):
        if i not in correctdex:
            ycorrect.append(ymat[i][1:-2])


    return bestxmat, ycorrect
    


def area_normalization(ymat):
    print "Normalizing Spectra"

    #'integrate' each spectrum
    totals = []
    for i in range(len(ymat)):
        total = 0
        for j in range(len(ymat[i])):
            total += ymat[i][j]
        totals.append(total)

    #normalize each spectrum by its integral
    norm_ymat = []
    for i in range(len(ymat)):
        yvec = []
        for j in range(len(ymat[i])):
            yvec.append(ymat[i][j]/float(totals[i]))
        norm_ymat.append(yvec)

    return norm_ymat

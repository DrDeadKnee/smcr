import scipy.io
import os
import configs
import numpy
from ui import ask


class WriteSpecs():

    def __init__(self,  datafolder, fileformat, infilenames):
        self.outform = fileformat
        self.foutpath = datafolder + "/Results"

        if not os.path.exists(self.foutpath):
            os.mkdir(self.foutpath)

        self.txtinnames = infilenames

    def write_spectra(self, xvalues, yvalues):
        if self.outform == "mat":
            self.write_matlab_matrix(xvalues, yvalues)
        else:
            self.write_generic(xvalues, yvalues)

    def write_matlab_matrix(self, xvalues, yvalues):
        outfilename = ask("What is the output matrix called?\n>", str())
        outmatrix = self.foutpath + "/" + outfilename + ".mat"
        ymat = numpy.matrix(yvalues)
        scipy.io.savemat(outmatrix, mdict={"D": ymat})

    def write_generic(self, xvalues, yvalues):
        print("werite generic not yet built.  NO OUTPUT")


class LoadSpecs():

    def __init__(self, datafolder):
        self.NOPROBLEMS = True
        self.input_folder = datafolder
        self.xdata, self.ydata, self.filenames = self._import_spectra()

    def get_rawdata(self):
        return self.xdata, self.ydata, self.filenames

    def _import_spectra(self):
        # grab the filenames
        filenames = os.listdir(self.input_folder)
        txtfiles = []
        for i in filenames:
            if ".txt" in i:
                txtfiles.append(i)

        # construct datamatrices
        xdata = []
        ydata = []
        for i in txtfiles:
            xvals, yvals = self._import_spectrum(i)
            xdata.append(xvals)
            ydata.append(yvals)

        # check xvalues
        for i in range(len(xdata)):
            print("Importing file {}".format(txtfiles[i]))
            for k in range(len(xdata[i])):
                if xdata[0][k] != xdata[i][k]:
                    self.NOPROBLEMS = False
            if not self.NOPROBLEMS:
                print("Problem with training set. File %s xvalues unequal to %s." % (txtfiles[i], txtfiles[0]))
                self.NOPROBLEMS = True
                for k in range(len(xdata[i])):
                    if ((xdata[0][k] - xdata[i][k])**2)**0.5 > configs.get("xdif"):
                        self.NOPROBLEMS = False

        ut8txt = [name.decode('utf-8') for name in txtfiles]

        return xdata, ydata, ut8txt  # txtfiles

    def _import_spectrum(self, filename):
        fin = self.input_folder + "/" + filename
        findata = open(fin)

        xvals = []
        yvals = []
        for i in findata:
            finvals = i.split()
            if len(finvals) == 2:
                xvals.append(float(finvals[0]))
                yvals.append(float(finvals[1]))

        return xvals, yvals

import configs
import numpy as np
from scipy.interpolate import interp1d


def vertical_offset(xmat, ymat):
    # ensures all spectra begin at zero. As ben-amotz would say, "no negative inputs"
    print("\n  Offsetting spectral intensity such that min(spectrum) = 0")
    shifted_ymat = []
    for i in range(len(ymat)):
        shifted_spectrum = []
        miny = min(ymat[i])
        for j in range(len(ymat[i])):
            shifted_spectrum.append(ymat[i][j] - miny)
        shifted_ymat.append(shifted_spectrum)

    return xmat, shifted_ymat


def clip_spectra(xmat, ymat):
    xmin = configs.get("x_minimum")
    xmax = configs.get("x_maximum")

    Xred = []
    Yred = []
    for i in range(len(xmat)):
        X = np.asarray(xmat[i])
        Y = np.asarray(ymat[i])

        m1 = np.greater(X, xmin)
        m2 = np.less(X, xmax)
        mask = m1 & m2

        Xred.append(X[mask].tolist())
        Yred.append(Y[mask].tolist())

    return Xred, Yred


def align_spectra(xmat, ymat):
    print("\n  Aligning Spectra. Note that this will truncate the dataset by the lowest and higest values.")

    # find all spectra with identical x values, and track their indices
    x_begins = [[1]]
    for i in range(len(xmat)):
        counted = False
        for j in range(len(x_begins)):
            if x_begins[j][0] == xmat[i][0]:
                counted = True
                x_begins[j].append(i)
        if not counted:
            x_begins.append([xmat[i][0], i])

    # determine which set of x indixes to use
    nspecswithx = []
    for i in range(len(x_begins)):
        nspecswithx.append(len(x_begins[i]) - 1)
    bestxlist = xmat[x_begins[nspecswithx.index(max(nspecswithx))][1]][1: -2]
    bestxmat = []
    for i in range(len(xmat)):
        bestxmat.append(bestxlist)

    # get an array with nothing but indices marked for correction
    del x_begins[nspecswithx.index(max(nspecswithx))]
    del x_begins[0]
    for i in x_begins:
        del i[0]

    correctdex = []
    for i in range(len(x_begins)):
        for j in range(len(x_begins[i])):
            correctdex.append(x_begins[i][j])

    # interpolate y values for marked spectra with cubic spline
    cs_fits = []
    for i in correctdex:
        cs = interp1d(xmat[i], ymat[i], kind="cubic", bounds_error="false")
        cs_fits.append(cs)

    # generate interpolated y values for marked spectra using the best x spectrum.
    ycorrect = []
    for i in range(len(cs_fits)):
        ycorrect.append(cs_fits[i](bestxlist))
    for i in range(len(ymat)):
        if i not in correctdex:
            ycorrect.append(ymat[i][1:-2])

    return bestxmat, ycorrect


def area_normalization(ymat):
    print("\n  Implementing unit area normalization.")

    # 'integrate' each spectrum
    totals = []
    for i in range(len(ymat)):
        total = 0
        for j in range(len(ymat[i])):
            total += ymat[i][j]
        totals.append(total)

    # i normalize each spectrum by its integral
    norm_ymat = []
    for i in range(len(ymat)):
        yvec = []
        for j in range(len(ymat[i])):
            yvec.append(ymat[i][j] / float(totals[i]))
        norm_ymat.append(yvec)

    return norm_ymat

import nimfa
import configs
import numpy as np
from math import fabs
from matplotlib import pyplot as plt


class Factorize():

    def __init__(self):
        pass

    def domethod(self, methodname, ymat_raw):
        ymat = np.transpose(ymat_raw)
        possible_components = None

        if methodname != "Lawton":
            if methodname == "BMF":
                W, H = self.BMF_method(ymat)
            elif methodname == "SEPNMF":
                W, H = self.SEPNMF_method(ymat)
            elif methodname == "NMF":
                W, H = self.NMF_method(ymat)

            default_components = W.tolist()
            default_loadings = H.tolist()

        elif methodname == "Lawton":
            possible_components, default_components, default_loadings = self.lawton_s_method(ymat)
        return possible_components, default_components, default_loadings

    def BMF_method(self, ymat):
        V = ymat
        factors = configs.get("component_number")

        # factorize
        bmf = nimfa.Bmf(V, max_iter=200000, rank=factors, track_error=True)
        bmf_fit = bmf()

        # retrieve basis and fit coefficients
        W = np.transpose(bmf_fit.basis())
        H = bmf_fit.coef()

        # return to list format
        components = W.tolist()
        loadings = H.tolist()

        return components, loadings

    def SEPNMF_method(self, ymat):
        V = ymat
        factors = configs.get("component_number")

        # factorize
        sepnmf = nimfa.SepNmf(V, max_iter=20000, rank=factors, track_error=True)
        sepnmf_fit = sepnmf()

        # retrieve basis and fit coefficients
        W = np.transpose(sepnmf_fit.basis())
        H = sepnmf_fit.coef()

        # return to list format
        components = W.tolist()
        loadings = H.tolist()

        return components, loadings

    def NMF_method(self, ymat):
        V = ymat
        factors = configs.get("component_number")

        # factorize
        nmf = nimfa.Nmf(V, max_iter=5000, rank=factors)
        nmf_fit = nmf()

        # retrieve basis and fit coefficients
        W = np.transpose(nmf_fit.basis())
        H = nmf_fit.coef()

        # return to list format
        components = W.tolist()
        loadings = H.tolist()

        return components, loadings

    def lawton_s_method(self, ymat):
        # implements the method laid out but Lawton and Sylvestre:
        # William H. Lawton & Edward A. Sylvestre (1971),
        # Self Modeling Curve Resolution, Technometrics, 13:3, 617-633
        # regions, lines, etc. reference figure 2

        # set up second-moment matrix, M
        Y, Yprime, n = np.transpose(ymat), ymat, len(ymat[0])
        M = np.dot(Yprime, Y) / float(n)

        # find primary eigenvectors, V1 and V2
        eigenvalues, eigenvectors = np.linalg.eigh(M)
        eigenvectors = np.transpose(eigenvectors)
        V1, V2 = eigenvectors[-1], eigenvectors[-2]
        print("Five highest normalized eigenvalues")
        for i in eigenvalues[-5:]:
            print(i / np.sum(eigenvalues))

        if V1[np.argmax(np.absolute(V1))] < 0:
            V1 *= -1
        # find the reconstruction elements, xi[i1] and xi[i2]
        xii1, xii2 = [], []
        for i in range(len(Y)):
            xii1.append(np.dot(Y[i], V1))
            xii2.append(np.dot(Y[i], V2))

        # find the outer bounds for regions 1 and 2 using eq.20
        # find the values tau and zeta
        zeta_candidates = []
        tau_candidates = []
        for k in range(len(V1)):
            if V2[k] >= 0:
                zeta_candidates.append(fabs(V1[k] / float(V2[k])))
            elif V2[k] < 0:
                tau_candidates.append(fabs(V1[k] / float(V2[k])))
        zeta = -1 * min(zeta_candidates)
        tau = min(tau_candidates)
        region1_outerslope = tau
        region2_outerslope = zeta

        # find inner bounds for regions 1 and 2 as d(xi2)/d(xi1)
        # evaluated at "most pure" spectra
        # mp2 is pure water, mp1 is other
        mp1_idx = xii2.index(max(xii2))
        mp2_idx = xii2.index(min(xii2))
        region1_innerslope = xii2[mp1_idx] / xii1[mp1_idx]
        region2_innerslope = xii2[mp2_idx] / xii1[mp2_idx]

        # find where the line given by eq. 25 intersects with
        # inner and outer bounds. Intesections of form [xi1, xi2]
        c1 = sum(V1)
        c2 = sum(V2)
        r1_outersect = [1 / (c1 + c2 * region1_outerslope), region1_outerslope / (c1 + c2 * region1_outerslope)]
        r1_innersect = [1 / (c1 + c2 * region1_innerslope), region1_innerslope / (c1 + c2 * region1_innerslope)]
        r2_innersect = [1 / (c1 + c2 * region2_innerslope), region2_innerslope / (c1 + c2 * region2_innerslope)]
        r2_outersect = [1 / (c1 + c2 * region2_outerslope), region2_outerslope / (c1 + c2 * region2_outerslope)]

        # calulate f* solution bands given by xi1, xi2 intersections
        # and primary eigenvectors
        r1_outerspec = V1 * r1_outersect[0] + V2 * r1_outersect[1]  # fi*
        r1_innerspec = V1 * r1_innersect[0] + V2 * r1_innersect[1]  # fi**
        r2_innerspec = V1 * r2_innersect[0] + V2 * r2_innersect[1]  # fii**
        r2_outerspec = V1 * r2_outersect[0] + V2 * r2_outersect[1]  # fii*

        # assuming r1_innerspec = fi** to be the pure component 1
        # we have that a = 0 in eq.27
        # therefore component 2 is fii*.
        # If more variation in TMAO, this is bad.
        components = [r1_innerspec, r2_outerspec]
        loadings = []
        for i in range(len(Y)):
            a = self.vecregress(components[0], components[1], Y[i])
            loadings.append([a, 1 - a])

        specmat = [r1_outerspec, r1_innerspec, r2_innerspec, r2_outerspec]

        # store data to plot if requested
        self.eigvals = eigenvalues
        self.eigvecs = eigenvectors
        self.V1 = V1
        self.V2 = V2
        self.s1 = region1_outerslope
        self.s2 = region1_innerslope
        self.s3 = region2_innerslope
        self.s4 = region2_outerslope
        self.xi1 = xii1
        self.xi2 = xii2

        return specmat, components, loadings

    def vecregress(self, c1, c2, y):
        sumydelta = 0
        sum12 = 0
        sum22 = 0
        sumdeltadelta = 0

        for i in range(len(c1)):
            sumydelta += y[i] * (c1[i] - c2[i])
            sum12 += c1[i] * c2[i]
            sum22 += c2[i]**2
            sumdeltadelta += (c1[i] - c2[i])**2

        a = (sumydelta - sum12 + sum22) / sumdeltadelta
        return a

    def plot_lawt(self):
        plt.clf()
        plt.ioff()
        domain = [0, max(self.xi1) * 1.15]
        plt.xlim(0, domain[1])
        plt.xlabel("xi 1")
        plt.ylabel("xi 2")

        # get constraint lines
        xvals = np.arange(0, domain[1], step=(domain[1] - domain[0]) / 10.0)
        y1 = xvals * self.s1
        y2 = xvals * self.s2
        y3 = xvals * self.s3
        y4 = xvals * self.s4

        plt.plot(xvals, y1, color="blue")
        plt.plot(xvals, y2, color="green")
        plt.plot(xvals, y3, color="green")
        plt.plot(xvals, y4, color="blue")
        plt.plot(xvals, np.zeros(len(xvals)), color="black")

        # get intersecting line
        l1 = np.sum(self.V1)
        l2 = np.sum(self.V2)
        ymin = min([min(y1), min(y4)])
        ymax = max([max(y1), max(y4)])

        # yvals = np.arange(ymin, ymax, step=(ymax - ymin) / 10.0)
        yvals = np.asarray([ymin, ymax])
        xvals = 1 / l1 - (yvals * l2) / l1
        plt.plot(xvals, yvals, color="orange")

        for i in range(len(self.xi1)):
            plt.plot(self.xi1, self.xi2, "ro")
        plt.show()

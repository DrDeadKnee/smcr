import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from factorize import Factorize
import data_preprocessing
from curve_qc import QualityControl
from ui import ask


class CVBeta(object):

    def __init__(self, data, vis=True, write=False):
        self.raw_xdata, self.raw_ydata, self.names = self.parse_raw_dataframe(data)
        self.findfactors = Factorize()
        self.vis = vis
        self.betacount = 0
        self.altmethod = False
        self.allresults = {}
        sns.set_style('ticks')

    def run_cross_validation(self):
        self.seperate_training_data()
        possible_comps, default_comps, default_loadings = self.findfactors.domethod(
            "Lawton", self.ypretty
        )
        self.one_hunred_betas(possible_comps, "cross_validation")

    def run_truespec_regression(self, conc, dbh, compare=1):
        self.xpretty, self.ypretty = self.raw_xdata, self.raw_ydata
        qc = QualityControl(self.xpretty, self.ypretty, self.names)
        qc.ask_for_approval()
        self.xpretty, self.ypretty = qc.get_data()

        y1_star, y2_star = dbh.get_truespecs(conc)
        self.blank = np.asarray(y1_star['signal'])
        self.truesig = np.asarray(y2_star['signal'])
        self.blank /= np.sum(self.blank)
        self.truesig /= np.sum(self.truesig)

        possible_comps, default_comps, default_loadings = self.findfactors.domethod(
            "Lawton", self.ypretty
        )

        self.components = possible_comps
        if compare == 0:
            self.one_hunred_betas(possible_comps, "basic")
        elif compare == 1:
            self.one_hunred_betas(possible_comps, "truespec1")
        elif compare == 2:
            self.one_hunred_betas(possible_comps, "truespec2")
        elif compare == 3:
            self.one_hunred_betas(possible_comps, 'fixwat')

    def get_allresults(self):
        return self.allresults

    def seperate_training_data(self):
        self.xpretty, self.ypretty = data_preprocessing.clip_spectra(self.raw_xdata, self.raw_ydata)
        # self.ypretty = np.asarray([i - np.amin(i) for i in self.ypretty])
        self.ypretty = data_preprocessing.area_normalization(self.ypretty)

        qc = QualityControl(self.xpretty, self.ypretty, self.names)
        qc.ask_for_approval()
        qc.ask_for_blank()
        self.blank = np.asarray(qc.blank)
        self.xpretty, self.ypretty = qc.get_data()

    def truevaluate1comp(self, c1, c2, beta, symmetric=False, q=True):
        y1 = self.blank
        y2 = self.truesig
        c1 /= np.sum(c1)
        c2 /= np.sum(c2)

        if not self.altmethod:  # symmetric:
            D2 = np.dot(y2, np.log(y2 / y1)) + np.dot(y1, np.log(y1 / y2))
            D1 = np.dot(y2, np.log(y2 / c1)) + np.dot(c1, np.log(c1 / y2))

        else:
            D1 = np.dot(y2, np.log(y2 / c1))
            D2 = np.dot(y2, np.log(y2 / y1))

        if self.vis:
            self.plot_frame(c1, c2, self.blank, self.truesig, beta=beta)

        return D1, D2, np.abs(D1 - D2)

    def truevaluate2comp(self, c1, c2, beta, q=True):
        y1 = self.blank
        y2 = self.truesig
        c1 /= np.sum(c1)
        c2 /= np.sum(c2)

        if self.altmethod:  # triangulate:
            D1 = np.abs(np.dot(y2, np.log(y2 / c1)) + np.dot(c1, np.log(c1 / y2)))
            D2 = np.abs(np.dot(y1, np.log(y1 / c2)) + np.dot(c2, np.log(c2 / y1)))
        else:
            D1 = np.abs(np.dot(y1, np.log(y1 / c1)) + np.dot(c1, np.log(c1 / y1)))
            D2 = np.abs(np.dot(y2, np.log(y2 / c2)) + np.dot(c2, np.log(c2 / y2)))

        '''
        if q is True:
            H12 = -np.sum(y2 * (np.log(y1) - np.log(c1)))
            H21 = -np.sum(y1 * (np.log(y2) - np.log(c2)))

        elif q is False:
            H12 = -np.sum(y2 * (-np.log(y1) + np.log(c1)))
            H21 = -np.sum(y1 * (-np.log(y2) + np.log(c2)))

        H_joint = np.abs(H12) + np.abs(H21)
        '''

        if self.vis:
            self.plot_frame(c1, c2, self.blank, self.truesig, beta=beta)

        # return H12, H21, H_joint
        return D1, D2, D1 + D2

    def shortest_distance(self, c1, c2, beta):
        y1 = self.blank
        c1 /= np.sum(c1)

        RSS = np.sum((y1 - c1)**2)
        DKL = np.absolute(np.dot(c1, np.log(c1 / y1)) + np.dot(y1, np.log(y1 / c1)))

        if self.vis:
            self.plot_frame(c1, c2, self.blank, self.truesig, beta=beta)

        return RSS, DKL

    def one_hunred_betas(self, components, method, treatment=None, betabounded=False, write=False):
        if treatment == 'subtraction':
            self.blank -= np.amin(self.blank)
            components = [c - np.amin(c) for c in components]

        if not betabounded:
            # betas = np.arange(-0.3, 1.3, 0.01)
            betas = np.arange(-1, 2, 0.01)
        else:
            betas = np.arange(0, 1, 0.01)
        # betas = [1]
        betas = np.arange(0, 1, 0.01)
        RSS = []
        DKL = []
        D1 = []
        D2 = []
        Db = []

        for i in betas:
            c1, c2 = self.get_comps(components, i)
            if method == "cross_validation":
                c1s, c2s = self.get_comps(components, 1)
                rss, dkl = self.evaluate_singlecomp(c1, c2s, i, method="area")
                RSS.append(rss)  # ||y1 - c1||
                DKL.append(dkl)  # symmetric divergence between y1 and c1

            elif method == "truespec1":
                d1, ds, db = self.truevaluate1comp(c1, c2, i, q=False)
                D1.append(d1)  # symmetric divergence between y1 and c2
                D2.append(ds)  # symmetric divergence between y1 and y2
                Db.append(db)  # |D1 - Ds|

            elif method == "truespec2":
                db1, db2, dbt = self.truevaluate2comp(c1, c2, i, q=False)
                D1.append(db1)  # |D1 - Ds|
                D2.append(db2)  # |D2 - Ds|
                Db.append(dbt)  # |D1 + D2 - 2Ds|

            elif method == "basic":
                rss, dkl = self.shortest_distance(c1, c2, i)
                RSS.append(rss)
                DKL.append(dkl)

            elif method == "fixwat":
                rss, dkl = self.get_fixwat(c1, c2, components)
                RSS.append(rss)
                DKL.append(dkl)
                print('With beta={},   dkl={},  rss={}'.format(i, dkl, rss))

        if method == "cross_validation" or method == "basic" or method == 'fixwat':
            self.display_cv(components, RSS, DKL, betas)

        elif method == "truespec1":
            self.display_true1(components, D1, D2, Db, betas)

        elif method == "truespec2":
            self.display_true2(components, D1, D2, Db, betas)

    def get_fixwat(self, c1, c2, components):
        candidates = []
        for i in components:  # self.ypretty:
            candidates.append(np.dot(i - self.blank, i - self.blank))
        # water = self.ypretty[np.argmax(candidates)]
        water = components[np.argmin(candidates)]
        water /= np.sum(water)
        # water = self.blank / np.sum(self.blank)
        
        c2 /= np.sum(c2)
        dkl = []
        rss = []
        a = []
        b = []
        dka = []
        dkb = []

        for i in self.ypretty:
            spec = i / np.sum(i)
            best_fit, b_a, b_b, Adif, Bdif = self.optimize_dkl(spec, water, c2)
            fit = water * b_a + c2 * b_b
            dkl.append(best_fit)
            rss.append(np.dot(spec - fit, spec - fit))
            a.append(b_a)
            b.append(b_b)
            dka.append(Adif)
            dkb.append(Bdif)

            '''plt.clf()
            plt.plot(spec, label='spec')
            plt.plot(fit, label='fit')
            plt.plot(water, label='blank')
            plt.legend()
            plt.show()
            ask("moo", "s")'''
        dkl = np.asarray(dkl)
        rss = np.asarray(rss)
        b = np.asarray(b)
        a = np.asarray(a)
        aweight = a / np.sum(a)
        bweight = b / np.sum(b)
        # corkl = np.dot(aweight, np.asarray(dka)) + np.dot(bweight, np.asarray(dkb))
        # dkl = self.kldiv(water, c2) + self.kldiv(c2, water)
        # rss = np.dot(c2 - water, c2 - water)
        # align c2 with the hydrocarbon spectrum
        x = np.asarray(self.xpretty[0])
        m1 = x < 3075
        m2 = x > 3000
        M = m1 & m2
        locx = np.argmin(np.absolute(x - 3000))

        cpeak = np.argmax(c2[M]) + locx
        tpeak = np.argmax(self.truesig[M]) + locx
        peakdif = cpeak - tpeak

        # normc = c2 / c2[cpeak]
        # normt = self.truesig / self.truesig[tpeak]

        normc = c2 / np.sum(c2)
        normt = self.truesig / np.sum(self.truesig)

        if peakdif > 0:
            normc = normc[peakdif:]
            normt = normt[:-peakdif]
        elif peakdif < 0:
            normc = normc[:peakdif]
            normt = normt[-peakdif:]
        # difvec = normc - normt

        normc = normc - np.amin(normc)
        normt = normt - np.amin(normt)
        normc = c2 / np.sum(c2) + 10**-6
        normt = normt / normt + 10**-6

        plt.clf()
        plt.title('normvecs')
        plt.plot(normt)
        plt.plot(normc)
        # plt.plot(difvec)
        plt.show()
        ask("moo")
        w1, w2 = self.get_weights(c1, c2)
        fit = w1 * c1 + w2 * c2
        rss = np.sum((self.blank - fit)**2)

        dkl = np.sum(dkl) + (0.5 * self.kldiv(normc, normt) + 0.5 * self.kldiv(normt, normc))
        rss += np.dot(normc - normt, normc - normt)

        return rss, dkl

    def optimize_dkl(self, data, c1, c2):
        dkl = []
        A = []
        B = []
        for i in np.arange(0, 1, 0.01):
            a = 1 - i
            b = i
            p = data
            q = a * c1 + b * c2
            dkl.append(self.kldiv(p, q))
            A.append(a)
            B.append(b)
            '''plt.clf()
            print(self.kldiv(p, q))
            plt.plot(data)
            plt.plot(q)
            plt.show()
            ask("moo", "s")'''
        loc = np.argmin(dkl)
        DKL, a, b = dkl[loc], A[loc], B[loc]
        DKLa = self.kldiv(p, a * c1)
        DKLb = self.kldiv(p, a * c2)

        return DKL, a, b, DKLa, DKLb

    def display_true1(self, components, D1, D2, DB, betas):
        D1 = np.asarray(D1)
        D2 = np.asarray(D2)
        DB = np.asarray(DB)
        # m1, m2 = betas < 1.0, betas > 0.0
        # M = m1 & m2

        bb1 = betas[np.argmin(D1)]
        # bb2 = betas[np.argmin(D2)]
        bb = betas[np.argmin(DB)]
        bbx = np.argmax(DB)
        c1X, c2X = self.get_comps(components, bbx)

        c1, c2 = self.get_comps(components, bb)
        y1, y2 = self.blank, self.truesig

        print("D1=|D(c1||y2) + D(y2||c2)|={}".format(bb1))
        print("D1 + D2={}".format(bb))
 
        sns.set_context("paper", font_scale=1.5, rc={'lines.linewidth': 2})
        plt.clf()
        x = self.xpretty[0]
        plt.figure(1, figsize=(9.5, 9.5))
        plt.subplot(211)
        plt.ylabel("Component 1")
        plt.plot(x, y1, label="Y1*")
        plt.plot(x, c1, label="C1*")
        plt.legend(loc="best")

        plt.subplot(212)
        plt.plot(x, y2, label="Y2*")
        plt.plot(x, c2, label="C2*")
        # plt.plot(x, c2X, label="max(H(y2,c1/y1))")
        plt.ylabel("Component 2")
        plt.xlabel("Wavenumber (1/cm)")
        plt.show()
        ask("moo", 's')

        plt.clf()
        plt.figure(1, figsize=(9.5, 13))
        plt.subplot(211)
        plt.plot(betas, D1, label="D(y2|c1)")
        plt.plot(betas, D2, label="D(y2|y1)")
        plt.ylabel("D1")
        plt.legend(loc='best')

        plt.subplot(212)
        plt.plot(betas, DB)
        plt.ylabel(r'$D_{KL}$')
        plt.xlabel(r'$\beta_b$')
        plt.show()
        ask("moo", 's')

        if self.altmethod:
            m = 5
        else:
            m = 2

        self.write_results(bb, c1, c2, m)

    def write_results(self, beta, c1, c2, method):
        methodex = [
            [],
            ['best_beta_m1', 'spec1_m1', 'spec2_m1'],
            ['best_beta_m2', 'spec1_m2', 'spec2_m2'],
            ['best_beta_m3', 'spec1_m3', 'spec2_m3'],
            ['best_beta_m4', 'spec1_m4', 'spec2_m4'],
            ['best_beta_m5', 'spec1_m5', 'spec2_m5'],
        ]

        self.results = {
            'beta': beta,
            'c1': c1,
            'c2': c2,
            'wavenumber': self.xpretty[0],
            'method': method
        }
        results = self.results

        halfresults = {
            'wavenumber': results['wavenumber'],
            "{}".format(methodex[results['method']][1]): results['c1'],
            "{}".format(methodex[results['method']][2]): results['c2'],
            "{}".format(methodex[results['method']][0]): results['beta']
        }
        self.allresults.update(halfresults)
        # print(self.allresults)

    def display_true2(self, components, D1, D2, DB, betas):
        D1 = np.asarray(D1)
        D2 = np.asarray(D2)
        DB = np.asarray(DB)
        # m1, m2 = betas < 1.0, betas > 0.0
        # M = m1 & m2

        bb1 = betas[np.argmin(D1)]
        bb2 = betas[np.argmin(D2)]
        bb = betas[np.argmin(DB)]

        c1, c2 = self.get_comps(components, bb)
        y1, y2 = self.blank, self.truesig

        print("D1=|D(c1|y2) - D*|={}".format(bb1))
        print("D2=|D(c2|y1) - D*|={}".format(bb2))
        print("D1 + D2={}".format(bb))

        sns.set_style("ticks")
        sns.set_context("paper", font_scale=1.5, rc={'lines.linewidth': 2})
        plt.clf()

        x = self.xpretty[0]

        plt.figure(1, figsize=(9.5, 9.5))
        plt.subplot(211)
        plt.ylabel("Component 1")
        plt.plot(x, y1, label="Y1*")
        plt.plot(x, c1, label="C1*")
        plt.legend(loc="best")

        plt.subplot(212)
        plt.plot(x, y2, label="Y2*")
        plt.plot(x, c2, label="C2*")
        plt.ylabel("Component 2")
        plt.xlabel("Wavenumber (1/cm)")
        plt.show()

        ask("moo", 's')

        plt.clf()
        plt.figure(1, figsize=(9.5, 13))
        plt.subplot(311)
        plt.plot(betas, D1)
        plt.ylabel("D1")

        plt.subplot(312)
        plt.plot(betas, D2)
        plt.ylabel("D2")

        plt.subplot(313)
        plt.ylabel("D1 + D2")
        plt.plot(betas, DB)
        plt.xlabel("Beta")
        plt.show()

        ask("moo", 's')
        if self.altmethod:
            m = 3
        else:
            m = 4
        self.write_results(bb, c1, c2, m)

    def display_cv(self, components, RSS, DKL, betas):
        DKL = np.asarray(DKL)
        RSS = np.asarray(RSS)

        bbrss = betas[np.argmin(RSS)]
        if bbrss == betas[-1]:
            bbrss = betas[-1]
        bbdkl = betas[np.argmin(DKL)]
        if bbdkl == betas[-1]:
            bbdkl = betas[-1]

        c1rss, c2rss = self.get_comps(components, bbrss)
        c1dkl, c2dkl = self.get_comps(components, bbdkl)

        print("RSS={}".format(bbrss))
        print("DKL={}".format(bbdkl))
        plt.clf()

        x = self.xpretty[0]

        sns.set_context("paper", font_scale=2, rc={'lines.linewidth': 2})
        plt.figure(1, figsize=(8, 12))

        plt.subplot(211)
        plt.ylabel("Component 1")
        plt.plot(x, c1rss, label="RSS")
        plt.plot(x, c1dkl, label="DKL")
        plt.plot(x, self.blank, label="Blank")
        plt.legend(loc="best")

        plt.subplot(212)
        plt.plot(x, c2rss, label="RSS")
        plt.plot(x, c2dkl, label="DKL")
        plt.plot(x, self.truesig, label='Blank')
        plt.ylabel("Component 2")
        plt.xlabel("Wavenumber (1/cm)")
        plt.show()
        ask("moo", 's')

        plt.clf()
        # plt.title("Metrics")
        plt.figure(1, figsize=(9.5, 13))
        plt.subplot(211)
        plt.ylabel(r'$\mathrm{RSS}$')
        plt.plot(betas, RSS)

        plt.subplot(212)
        plt.ylabel(r'$D_{KL}$')
        plt.plot(betas, DKL)
        plt.xlabel(r'$\beta_b$')
        plt.show()

        c1 = c1dkl
        c2 = c2dkl
        bb = bbdkl
        m = 1
        self.write_results(bb, c1, c2, m)

    def plot_frame(self, c1, c2, y1, y2, beta=None, plotevery=20):
        if beta is None:
            beta = self.betacount / 100.0
        if self.vis and self.betacount % plotevery == 0:
            plt.clf()
            sns
            plt.figure(1, figsize=(9.5, 9.5))
            plt.subplot(211)
            plt.plot(self.xpretty[0], c1, label="C1({})".format(beta), color="blue")
            plt.plot(self.xpretty[0], y1, label="Blank", color="orange")
            plt.ylabel("Component 1")
            plt.legend(loc="best")

            plt.subplot(212)
            plt.plot(self.xpretty[0], c2, label="C2({})".format(beta), color="#5454FF")
            plt.plot(self.xpretty[0], y2, label="BenzeneEthanol", color="#F5A50F")
            plt.legend(loc="best")
            plt.ylabel("Component 2")
            plt.xlabel("wavenumber (1/cm)")
            plt.show()
            ask("Beta = {}", 's')

        self.betacount += 1

    def evaluate_singlecomp(self, c, c2, beta, method="area"):
        if method == "top":
            p = self.blank / np.amax(self.blank)
            q = c / np.amax(c)

        elif method == "area":
            p = self.blank / np.sum(self.blank)
            q = c / np.sum(c)

        if self.vis:
            self.plot_frame(p, q)

        rss = np.sum((p - q)**2)
        Dpq = self.kldiv(p, q)

        return rss, Dpq

    def evaluate_spectra(self, c1, c2):
        # fit RSS
        w1, w2 = self.get_weights(c1, c2)
        fit = w1 * c1 + w2 * c2
        rss = np.sum((self.blank - fit)**2)

        # fit DKL
        w1, w2 = self.get_KL_weights(c1, c2)
        fit = w1 * c1 + w2 * c2

        '''plt.clf()
        plt.plot(c1, label="c1")
        plt.plot(c2, label="c2")
        plt.plot(self.blank, label="blank")
        plt.legend(loc="best")
        plt.show()
        raw_input("beta=?")'''

        dkl = self.kldiv(self.blank, fit)
        return rss, dkl

    def get_KL_weights(self, c1, c2):
        allw = np.arange(0, 1, 0.01)
        divs = []
        for j in allw:
            q = j * c1 + (1 - j) * c2
            divs.append(self.kldiv(self.blank, q))
        divs = np.asarray(divs)
        w1 = allw[np.argmin(divs)]
        w2 = 1 - w1

        return w1, w2

    def kldiv(self, p, q):
        D = np.sum(p * (np.log(p) - np.log(q)))
        if not isinstance(D, float):
            D = 0
        return D

    def get_weights(self, c1, c2):
        dc = c1 - c2
        y = self.blank

        w1 = (np.dot(y, dc) - np.dot(c2, dc)) / np.dot(dc, dc)
        if w1 > 1.0:
            w1 = 1.0
        w2 = 1 - w1

        return w1, w2

    def get_comps(self, components, beta):
        # specmat = [r1_outerspec, r1_innerspec, r2_innerspec, r2_outerspec]
        s1 = self.fix_array(beta * components[0] + (1 - beta) * components[1])
        # s1 = self.fix_array(components[1])
        s2 = self.fix_array(beta * components[3] + (1 - beta) * components[2])

        rs1 = np.sum((self.blank / np.sum(self.blank) - components[0] / np.sum(components[0]))**2)
        rs2 = np.sum((self.blank / np.sum(self.blank) - components[3] / np.sum(components[3]))**2)

        n1 = self.fix_array(components[0])
        n2 = self.fix_array(components[3])
        ct = n1 / np.sum(n1)
        cb = n2 / np.sum(n2)
        y = self.blank / np.sum(self.blank)

        dkl1 = np.dot(y, np.log(y / ct)) + np.dot(ct, np.log(ct / y))
        dkl2 = np.dot(y, np.log(y / cb)) + np.dot(cb, np.log(cb / y))

        if rs1 < rs2 and dkl1 < dkl2:
            c1, c2 = s1, s2
        elif rs1 > rs2 and dkl1 > dkl2:
            c1, c2 = s2, s1
        else:
            print("There is confusion about component 1! Defaulting to c[0] = c1")
            c1, c2 = s1, s2

        return self.fix_array(np.asarray(c1)), self.fix_array(np.asarray(c2))

    def fix_array(self, a):
        if np.amin(a) <= 0:
            aout = a - np.amin(a) + 0.000000001
        else:
            aout = a
        return aout

    def parse_raw_dataframe(self, dataframe):
        filenums = dataframe['filenumber'].unique()
        xmat = []
        ymat = []
        names = []
        for number in filenums:
            mask = dataframe['filenumber'].isin([number])
            xmat.append(dataframe['wavenumber'][mask].tolist())
            ymat.append(dataframe['signal'][mask].tolist())
            names.append(dataframe['filename'][mask].tolist()[0])

        return xmat, ymat, names

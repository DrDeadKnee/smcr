import numpy as np
import pandas as pd
import sqlite3
from matplotlib import pyplot as plt
from ui import choose
import seaborn as sns
from scipy.interpolate import interp1d


class AllAlign(object):

    def __init__(self, path):
        self.cnx = sqlite3.connect(path)
        self.filemaster = pd.read_sql("SELECT * FROM FileMaster", self.cnx)
        sns.set_style("ticks")

    def align_all(self):
        left_bound, right_bound = self.check_boundaries()
        for set_id in np.unique(self.filemaster['set_id']):
            mask = self.filemaster['set_id'] == set_id
            for filenumber in self.filemaster['filenumber'][mask].dropna():
                if not self.record_exists(set_id, filenumber):
                    print("Aligning file {} from set {}".format(filenumber, set_id))
                    aligned_spec = self.align_one(set_id, filenumber, left_bound, right_bound)
                    self.write_dataframe(aligned_spec)

    def write_dataframe(self, dataframe):
        dataframe.to_sql("AlignData", con=self.cnx, index=False, if_exists="append")

    def record_exists(self, set_id, filenumber):
        sql = """
        SELECT count(1)
        FROM AlignData
        WHERE set_id={}
        AND filenumber={}
        """.format(
            set_id,
            filenumber
        )
        cur = self.cnx.cursor()
        response = [i for i in cur.execute(sql)][0][0]
        if response == 0:
            return False
        else:
            return True

    def align_one(self, set_id, filenumber, left_bound=None, right_bound=None):
        sql = "SELECT * FROM RawData WHERE set_id={} AND filenumber={}".format(set_id, filenumber)
        specdata = pd.read_sql(sql, con=self.cnx)

        # clip edges
        if left_bound is None:
            left_bound = np.amin(specdata['wavenumber'])
        if right_bound is None:
            right_bound = np.amin(specdata['wavenumber'])
        m1 = specdata['wavenumber'] > np.ceil(left_bound)
        m2 = specdata['wavenumber'] < np.floor(right_bound)
        M = m1 & m2
        specdata = specdata[M].dropna()

        # determine new waven, get basis, fit to new waven
        new_waven = np.arange(np.round(left_bound) + 5, np.round(right_bound) - 5)
        basis = interp1d(
            np.asarray(specdata['wavenumber']),
            np.asarray(specdata['signal']),
            kind="cubic"
        )
        new_signal = basis(new_waven)
        new_setID = np.asarray([set_id] * len(new_signal))
        new_fileID = np.asarray([filenumber] * len(new_signal))
        new_filename = np.asarray([specdata['filename'].tolist()[0]] * len((new_signal)))
        to_df = {
            'set_id': new_setID,
            'filenumber': new_fileID,
            'filename': new_filename,
            'signal': new_signal,
            'wavenumber': new_waven
        }

        df = pd.DataFrame.from_dict(to_df)
        return df

    def check_boundaries(self):
        sis = []
        fis = []
        smallwaves = []
        bigwaves = []
        for set_id in np.unique(self.filemaster['set_id']):
            mask = self.filemaster['set_id'] == set_id
            for filenumber in self.filemaster['filenumber'][mask].dropna():
                sql = """
                    SELECT wavenumber
                    FROM RawData
                    WHERE set_id={} AND filenumber={}
                """.format(set_id, filenumber)
                wavens = pd.read_sql(sql, self.cnx)
                smallwaves.append(np.amin(wavens['wavenumber']))
                bigwaves.append(np.amax(wavens['wavenumber']))
                sis.append(set_id)
                fis.append(filenumber)

        small = np.argmax(smallwaves)
        big = np.argmax(bigwaves)

        print("Smallest: w={}, setid={}, fileid={}".format(max(smallwaves), sis[small], fis[small]))
        print("Biggest: w={}, setid={}, fileid={}".format(min(bigwaves), sis[big], fis[big]))

        sql1 = "SELECT * FROM RawData WHERE set_id={} AND filenumber={}".format(sis[small], fis[small])
        sql2 = "SELECT * FROM RawData WHERE set_id={} AND filenumber={}".format(sis[big], fis[big])
        s1 = pd.read_sql(sql1, con=self.cnx)
        s2 = pd.read_sql(sql2, con=self.cnx)

        plt.ion()
        plt.plot(s1['wavenumber'], s1['signal'])
        plt.plot(s2['wavenumber'], s2['signal'])
        plt.show()

        choice = choose("Is this OK?", ['yes', 'no'])
        if choice != 'yes':
            print("Exiting...")
            exit()
        else:
            print("Proceeding to alignment.")
            return max(smallwaves), min(bigwaves)


if __name__ == "__main__":
    path = '/media/deshthedesher/Data/science/raman/benzet/benzet5.db'
    AA = AllAlign(path)
    AA.align_all()

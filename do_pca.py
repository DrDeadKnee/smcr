import sqlite3
import pandas as pd
import numpy as np


class DoPCA(object):

    def __init__(self, db):
        self.cnx = sqlite3.connect(db)
        self.files = pd.read_sql("SELECT * FROM FileMaster", self.cnx)

    def PCA_Everything(self):
        sets = np.unique(self.files["set_id"])
        allfileids = []
        setids = []
        for i in sets:
            m1 = self.files['set_id'] == i
            tconc = np.unique(self.files["tmao"][m1])
            for j in tconc:
                m2 = self.files['tmao'][m1] == j
                M = m1 & m2
                potential_IDS = self.files['filenumber'][M].dropna().tolist()
                if len(potential_IDS) > 2:
                    allfileids.append(potential_IDS)
                    setids.append(i)

        for idx, ids in enumerate(allfileids):
            vectors, weights = self.PCA(ids, setids[idx])
            for i, fileid in enumerate(ids):
                m1 = self.files['set_id'] == setids[idx]
                m2 = self.files['filenumber'] == i
                M = m1 & m2
                rowindex = self.files[M].index.tolist()[0]
                self.files["PCA3"][rowindex] = weights[i][0]
                self.files["PCA2"][rowindex] = weights[i][1]
                self.files["PCA1"][rowindex] = weights[i][2]

        self.files.to_sql("FileMaster", con=self.cnx, index=False, if_exists="replace")

    def PCA(self, file_ids, set_id):
        signals = []
        wavenumbers = []
        for i in file_ids:
            sql = "SELECT * FROM RawData WHERE filenumber={} AND set_id={}".format(i, set_id)
            df = pd.read_sql(sql, con=self.cnx)
            m = df['wavenumber'] > np.amin(df['wavenumber'])
            signals.append(df['signal'][m] / np.sum(df['signal'][m]).tolist())
            wavenumbers.append(df['wavenumber'][m].tolist())

        Y = np.asmatrix(signals)
        Cy = np.dot(Y.transpose(), Y) / float(len(Y))
        eigvals, eigvecs = np.linalg.eigh(Cy)
        V = eigvecs.transpose()[-3:]

        weights = self.get_weights(Y, V)
        return V, weights

    # Both data matrix and vecotr matrix assumed to be composed of row vectors
    def get_weights(self, data_matrix, vector_matrix):
        Y = np.asmatrix(data_matrix)
        X = np.asmatrix(vector_matrix)
        Xt = X.transpose()
        weights = []
        for i in range(len(Y)):
            a = np.dot(Y[i], Xt)
            b = np.dot(X, Xt)
            weights.append(np.dot(a, np.linalg.inv(b)).tolist()[0])

        return weights


if __name__ == "__main__":
    path = '/media/deshthedesher/Data/science/raman/benzet/benzet5.db'
    DPC = DoPCA(path)
    DPC.PCA_Everything()


def choose(query, options):
    #options is a list of strings
    print "\n(q to quit)"
    
    legit_choices=[]
    querydisplay = query
    for number, option in enumerate(options):
        optiondisplay = "\n" + str(number+1) + " - " + str(option)
        querydisplay += optiondisplay
        legit_choices.append(str(number+1))
    querydisplay += "\n>"
            
    choice = raw_input(querydisplay)


    if choice == "q":
        exit()

        
    if choice in legit_choices:
        legitchoice = True
    else:
        legitchoice = False

    if legitchoice:
        return options[int(choice)-1]

    else:
        print "That was not an option. Please enter one of the following:"
        for i in legit_choices:
            print i

        choice = choose(query, options)
        return choice


def ask(query, variabletype="string"):
    #flexible ask module which will take any specific type
    print "\n(q to quit)"
    querydisplay = query+"\n>"
    
    user_answer = raw_input(querydisplay)
    
    if user_answer == "q":
        exit()


    elif type(variabletype) == type(int()):
        try:
            processed_answer = int(user_answer)
        except ValueError:
            raw_input("Query requires a value of type int. Please try again. (any button to continue)\n>")
            processed_answer = ask(query, variabletype)
    
    elif type(variabletype) == type(str()):
        #don't know how to actually check this off the top of my head
        processed_answer = user_answer 

    elif type(variabletype) == type(float()):
        try:
            processed_answer = float(user_answer)
        except ValueError:
            raw_input("Query requires a value of type float. Please try again. (any button to continue)\n>")
            processed_answer = ask(query, variabletype)

    #TODO: add other types or reduce planned scope


    return processed_answer



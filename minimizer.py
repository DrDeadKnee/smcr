import numpy as np
from matplotlib import pyplot as plt


class Minimizer(object):

    # X and Y are data and component matrices
    def __init__(self, X=None, Y=None):
        if X is not None:
            self.X = self.normalize_matrix(X)
        self.Y = Y

    def get_weights(self, X=None, Y=None):
        self._set_xy(X, Y)
        X = self.X
        Y = self.Y
        W = []

        c1 = np.asarray(Y[0])
        c2 = np.asarray(Y[1])
        dc = c1 - c2

        for i in range(len(X)):
            w1 = (np.dot(X[i], dc) - np.dot(c2, dc)) / np.dot(dc, dc)
            if w1 > 1.0:
                w1 = 1.0
            W.append([w1, 1 - w1])

        return W

    def get_KL_weights(self, X=None, Y=None):
        self._set_xy(X, Y)
        X = self.X
        Y = self.Y
        W = []

        c1 = np.asarray(Y[0])
        c2 = np.asarray(Y[1])

        for i in range(len(X)):
            allw = np.arange(0, 1, 0.01)
            w_inv = np.sum(c2 / (X[i] * (c1 - c2))) / np.sum(1 / X[i])
            w = 1 / w_inv

            '''divs = []
            for j in allw:
                q = j * c1 + (1 - j) * c2
                divs.append(np.sum(X[i] * np.log(X[i] / q)))
            w = allw[np.argmin(divs)]'''
            W.append([w, 1 - w])

        return W

    def get_KL_div(self, W=None, X=None, Y=None):
        self._set_xy(X, Y)
        X = self.X
        Y = self.Y

        if W is None:
            W = self.get_weights()
        dkl = 0

        c1 = np.asarray(Y[0])
        c2 = np.asarray(Y[1])

        for i in range(len(X)):
            p = X[i]
            q = W[i][0] * c1 + W[i][1] * c2
            dkl += np.sum(p * np.log(p / q))

        return dkl

    def get_RSS(self, X=None, Y=None):
        self._set_xy(X, Y)
        X = self.X
        Y = self.Y
        W = self.get_weights()
        RSS = 0

        c1 = np.asarray(Y[0])
        c2 = np.asarray(Y[1])

        for i in range(len(X)):
            RSS += np.sum((X[i] - W[i][0] * c1 - W[i][1] * c2)**2)

        return RSS

    def runallbeta(self, allcomponents, X=None):
        allbeta = np.arange(0.01, 1, 0.01)
        DKL = []
        RSS = []

        for i in allbeta:
            c1, c2 = self.compute_components(i, allcomponents)
            self._set_xy(X, [c1, c2])
            """plt.clf()
            plt.ioff()
            plt.plot(c1)
            plt.plot(c2)
            plt.show()"""
            weights = self.get_KL_weights()
            RSS.append(self.get_RSS())
            DKL.append(self.get_KL_div(W=weights))

        low_DK = np.argmin(DKL)
        beta = allbeta[low_DK]
        c1, c2 = self.compute_components(beta, allcomponents)
        c11, c21 = self.compute_components(1, allcomponents)
        c12, c22 = self.compute_components(0.5, allcomponents)
        c13, c23 = self.compute_components(0, allcomponents)
        plt.clf()
        plt.ioff()
        plt.plot(c1, color='blue')
        plt.plot(c2, color='blue', label=r'KL Min')
        plt.plot(c21, color='green')
        plt.plot(c11, color='green', label="1")
        plt.plot(c22, color='red', label="0.5")
        plt.plot(c12, color="red")
        plt.plot(c23, color='purple', label='0')
        plt.plot(c13, color='purple')
        plt.plot(c21, color='red')

        plt.legend()
        plt.show()
        plt.plot(DKL)
        plt.show()

        return RSS, DKL

    def compute_components(self, beta, allcomponents):
        c1 = beta * allcomponents[0] + (1 - beta) * allcomponents[1]
        c2 = beta * allcomponents[3] + (1 - beta) * allcomponents[2]
        return c1, c2

    def _set_xy(self, X, Y):
        if X is not None:
            self.X = self.normalize_matrix(X)
        if Y is not None:
            self.Y = Y

    def normalize_matrix(self, matrix):
        nmat = []
        for i in matrix:
            nmat.append(np.asarray(i) / np.sum(i))

        return nmat

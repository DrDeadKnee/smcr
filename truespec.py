import sqlite3
import numpy as np
import pandas as pd
from curve_qc import QualityControl


class TrueSpec(object):

    def __init__(self, path):
        self.cnx = sqlite3.connect(path)
        self.filemaster = pd.read_sql("SELECT * FROM FileMaster", con=self.cnx)

    def run_filtration(self):
        tconcs = np.unique(self.filemaster['tmao'])
        for conc in tconcs:
            print("NOW ANALYZING {} M TMAO".format(conc))
            spec_signal, spec_waven, names = self.collect_spectra(conc)
            spec_signal, spec_waven, names = self.remove_duplicates(spec_signal, spec_waven, names)
            spec_signal, spec_waven = self.visual_filter(spec_signal, spec_waven, names)

            weights = self.get_weights(spec_signal, X=spec_waven, norm_num=3410)
            true_signal = self.merge_spectra(spec_signal, weights=weights)
            self.true_to_db(true_signal, spec_waven[0], conc)

    def true_to_db(self, signal, waven, conc):
        tmao = [conc] * len(signal)
        dictfordb = {"signal": signal, "wavenumber": waven, "tmao": tmao}
        df = pd.DataFrame.from_dict(dictfordb)
        df.to_sql("TrueSpecs", con=self.cnx, index=False, if_exists="append")

    def merge_spectra(self, spec_signal, weights=None):
        if weights is None:
            weights = np.asarray([1] * len(spec_signal))

        truespec = np.dot(weights.transpose(), spec_signal)
        return truespec

    def get_weights(self, Y, X=None, norm_num=None):
        w = []
        for i, spec in enumerate(Y):
            if norm_num is None:
                w.append((np.amax(spec) / np.amin(spec))**2)
            else:
                x_ind = np.argmin(X[i] - norm_num)
                w.append(spec[x_ind] / np.amin(spec))
        W = np.asarray(w) / max(w)

        return W

    def visual_filter(self, Y, X, names, xref=3410):
        qc = QualityControl(X, Y, names, norm_method="noiserat", xtoreference=xref)
        qc.ask_for_approval()
        newX,  newY = qc.get_data()
        return newY, newX

    def remove_duplicates(self, Y, X, names):
        indices = []
        for idx, specone in enumerate(Y):
            for jdx, spectwo in enumerate(Y):
                if np.sum(specone - spectwo) == 0:
                    indices.append([idx, jdx])
        realindices = []
        for i in indices:
            c1 = i[0] != i[1]
            c2 = i[0] not in realindices
            c3 = i[1] not in realindices
            if c1 and c2 and c3:
                realindices.append(i[0])

        for i in realindices:
            del Y[i]
            del X[i]
            del names[i]

        return Y, X, names

    def collect_spectra(self, tconc):
        if int(tconc) == 99:
            water = 0
            benzet = 100
        else:
            water = 1
            benzet = 0

        m1 = self.filemaster['tmao'] == tconc
        m2 = self.filemaster['water'] == water
        m3 = self.filemaster['benzet'] == benzet
        M = m1 & m2 & m3

        selected = self.filemaster[M]
        sets = selected['set_id'].tolist()
        files = selected['filenumber'].tolist()
        alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']

        spec_signal = []
        spec_waven = []
        names = []
        for idx, set_id in enumerate(sets):
            sql = """
                SELECT *
                FROM AlignData
                WHERE set_id={}
                AND filenumber={}
            """.format(
                set_id,
                files[idx]
            )
            data = pd.read_sql(sql, self.cnx)
            spec_signal.append(np.asarray(data['signal']))
            spec_waven.append(np.asarray(data['wavenumber']))
            names.append(alphabet[idx])

        return spec_signal, spec_waven, names


if __name__ == "__main__":
    path = '/media/deshthedesher/Data/science/raman/benzet/benzet5.db'
    TS = TrueSpec(path)
    TS.run_filtration()

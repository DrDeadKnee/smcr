from data_handler import DataHandler
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
from ui import ask


class BetaMax(object):

    def __init__(self, database, setid, allvis=False, procid=1, method=3):
        self.allvis = allvis
        sns.set_style('ticks')
        sns.set_context("paper", font_scale=1.5)
        dbh = DataHandler(database)
        self.rawdata = dbh.select_dataset("RawData", setid)
        self.compdata = dbh.select_dataset("Components", setid, proc_id=procid)
        plt.ion()

    def plot_five_betas(self):
        f = plt.figure(figsize=(8, 6))
        plt.xlabel("wavenumber")
        plt.ylabel("signal")

        betas = [0, 0.25, 0.5, 0.75, 1]
        for i in betas:
            plt.clf()
            plt.title("Beta = {}".format(i))
            c1, c2 = self.get_specs(i)
            plt.plot(c1, label="Species 1")
            plt.plot(c2, label="Species 2")
            plt.legend(loc="best")
            plt.show()
            ask("what now?", "s")

    def one_hunred_betas(self, method=3):
        betas = np.arange(0.01, 1, 0.01)
        F = []
        for i in betas:
            c1, c2 = self.get_specs(i)
            n = np.mean(c2)
            if method == 1:
                dkl1 = self.kldiv(c2, c1)
                dkl2 = self.kldiv(c2, n)
                F.append(dkl1 + dkl2)
            elif method == 2:
                dkl1 = self.kldiv(c1, c2)
                dkl2 = self.kldiv(n, c2)
                F.append(dkl1 + dkl2)
            elif method == 3:
                dkl1 = self.kldiv(n, c2)
                dkl2 = self.kldiv(c1, c2)
                F.append(i * (dkl1 + dkl2) + dkl2)
            elif method == 4:
                dkl1 = self.kldiv(c2, n)
                dkl2 = self.kldiv(c2, c1)
                F.append(i * (dkl1 + dkl2) + dkl2)
            elif method == 5:
                dkl1 = self.kldiv(n, c2)
                dkl2 = self.kldiv(c1, c2)
                F.append((i**0.5) * (dkl1 + dkl2) + dkl2)

        best_beta = betas[np.argmax(F)]
        c1, c2 = self.get_specs(best_beta)

        if self.allvis:
            plt.title('method = {}'.format(method))
            plt.xlabel('beta')
            plt.ylabel('F(beta)')
            plt.plot(betas, F)
            plt.show()

            plt.clf()
            plt.figure(figsize=(8, 6))
            plt.xlabel("wavenumber")
            plt.ylabel("signal")
            plt.title("Best Beta={}".format(best_beta))
            plt.plot(c1, label="Chemical Species 1")
            plt.plot(c2, label="Chemical Species 2")
            plt.show()
            plt.legend(loc="best")

        return c1, c2, F, best_beta

    def get_specs(self, beta):
        compdata = self.compdata
        c1 = beta * compdata['r1_inner'] + (1 - beta) * compdata['r1_outer']
        c2 = beta * compdata['r2_inner'] + (1 - beta) * compdata['r2_outer']
        return self.fix_array(np.asarray(c1)), self.fix_array(np.asarray(c2))

    def fix_array(self, a):
        if np.amin(a) <= 0:
            aout = a - np.amin(a) + 0.000000001
        else:
            aout = a
        return aout

    def kldiv(self, p, q):
        return np.sum(p * (np.log(p) - np.log(q)))


if __name__ == "__main__":
    database = '/media/deshthedesher/Data/science/raman/benzet/benzet_3db.db'
    setid = 1
    procid = 1
    bm = BetaMax(database, setid, allvis=True, procid=procid)
    bm.one_hunred_betas(method=5)
    bm.plot_five_betas()

import sqlite3
import pandas as pd
import os
from fileio import LoadSpecs
import numpy as np


class DataHandler(object):

    def __init__(self, db_path):
        self.path = db_path
        if os.path.isfile(db_path):
            self.cnx = sqlite3.connect(self.path)
        else:
            self.make_db()

    def dump_to_db(self, folder, tmao_conc):
        SpecGetter = LoadSpecs(folder)
        xdata, ydata, names = SpecGetter.get_rawdata()
        set_id = self.generate_metadata()

        self.dump_to_raw(xdata, ydata, names, set_id)
        self.dump_to_master(set_id, folder, tmao_conc, len(xdata))

    def update_filemaster(self, folder):
        master = self.get_master()
        mask = master['set_folder'] == folder
        set_id = master["set_id"][mask].tolist()[0]
        rawdata = self.select_dataset("RawData", set_id)

        df = pd.read_sql("SELECT * FROM FileMaster WHERE set_id=999", con=self.cnx)

        base_to_db = {
            'set_id': set_id,
            'tmao': master['tmao_conc'][mask].tolist()[0]
        }

        for i in np.unique(rawdata['filenumber']):
            self_to_db = base_to_db
            self_to_db['filename'] = np.unique(rawdata['filename'][rawdata['filenumber'] == i])[0]
            self_to_db['filenumber'] = i
            df = df.append(self_to_db, ignore_index=True)

        df.to_sql("FileMaster", con=self.cnx, index=False, if_exists="append")

    def get_master(self):
        return pd.read_sql("SELECT * FROM Master", con=self.cnx)

    def delete_dataset(self, set_id):
        # set_id = int(set_id)
        sql1 = "DELETE FROM Master WHERE set_id={}".format(set_id)
        sql2 = "DELETE FROM Components WHERE set_id={}".format(set_id)
        sql3 = "DELETE FROM RawData WHERE set_id={}".format(set_id)
        sql4 = "DELETE FROM FileMaster WHERE set_id={}".format(set_id)
        sql5 = "DELETE FROM AlignData WHERE set_id={}".format(set_id)

        cur = self.cnx.cursor()
        cur.execute(sql1)
        cur.execute(sql2)
        cur.execute(sql3)
        cur.execute(sql4)
        cur.execute(sql5)
        self.cnx.commit()
        cur.close()

    def dump_to_master(self, set_id, folder, tmao_conc, file_count):
        master = self.get_master()
        entry = {
            'set_id': set_id,
            'set_folder': folder,
            'tmao_conc': tmao_conc,
            'file_count': file_count
        }
        master = master.append(entry, ignore_index=True)
        master.to_sql("Master", con=self.cnx, index=False, if_exists="replace")

    def write_vectors(self, set_id, vectors):
        procid = self.generate_procid(set_id)
        setvec = []
        procvec = []

        for i in range(len(vectors[0])):
            setvec.append(set_id)
            procvec.append(procid)
        datamat = zip(setvec, procvec,  vectors[0], vectors[1], vectors[2], vectors[3])
        df = pd.DataFrame(datamat, columns=[
            'set_id',
            'proc_id',
            'r1_outer',
            'r1_inner',
            'r2_inner',
            'r2_outer'
        ])
        df.to_sql("Components", con=self.cnx, index=False, if_exists="append")

    def write_generic_results(self, tablename, data):
        highval = 0
        singdex = []
        for key in data:
            if isinstance(data[key], np.ndarray) or isinstance(data[key], list):
                length = len(data[key])
                if length > highval:
                    highval = length
            else:
                singdex.append(key)

        to_df = {}
        for key in data:
            if key in singdex:
                to_df[key] = [data[key] for i in range(highval)]
            elif isinstance(data[key], np.ndarray):
                difdex = [0 for i in range(highval - len(data[key]))]
                to_df[key] = np.insert(data[key], 1, difdex)
            elif isinstance(data[key], list):
                difdex = [0 for i in range(highval - len(data[key]))]
                to_df[key] = data[key] + difdex

        df = pd.DataFrame.from_dict(to_df)
        df.to_sql(tablename, con=self.cnx, index=False, if_exists="append")

    def update_master(self, set_id, dict_for_pd):
        master = self.get_master()
        proc_id = self.generate_procid(set_id)
        c1 = master["set_id"].isin([set_id])
        c2 = ~master['proc_id'].isin([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

        try:
            place = master[c1 & c2].index.tolist()[0]
            master['proc_id'][place] = proc_id
            for key in dict_for_pd:
                master[key][place] = dict_for_pd[key]

        except IndexError:
            line = master.isin([set_id]).index.tolist()[0]
            dict_for_pd['set_id'] = set_id
            dict_for_pd['set_folder'] = master['set_folder'][line]
            dict_for_pd['tmao_conc'] = master['tmao_conc'][line]
            dict_for_pd['file_count'] = master['file_count'][line]
            dict_for_pd['proc_id'] = proc_id
            master.append(dict_for_pd, ignore_index=True)

        print dict_for_pd
        master.to_sql("Master", con=self.cnx, index=False, if_exists="replace")

    def dump_to_raw(self, xdata, ydata, names, set_id):
        for i in range(len(xdata)):
            setids = []
            filenumbers = []
            filename = []
            for k in range(len(xdata[i])):
                setids.append(set_id)
                filenumbers.append(i)
                filename.append(names[i].replace(".txt", ""))

            data = zip(setids, filenumbers, filename, xdata[i], ydata[i])
            smalldf = pd.DataFrame(
                data,
                columns=['set_id', 'filenumber', 'filename', 'wavenumber', 'signal']
            )

            smalldf.to_sql("RawData", con=self.cnx, index=False, if_exists="append")

    def select_dataset(self, tablename, set_id, proc_id=None):
        if proc_id is None:
            sql = "SELECT * FROM {} where set_id={}".format(tablename, set_id)
        elif proc_id is not None:
            sql = "SELECT * FROM {} where set_id={} and proc_id={}".format(tablename, set_id, proc_id)
        return pd.read_sql(sql, con=self.cnx)

    def make_db(self):
        schema_name = os.getcwd() + "/data_schema.sql"
        schema = open(schema_name, "r").read()
        commands = schema.replace("\n", "").split(";")
        filtered_commands = [i for i in commands if "COMMIT" not in i]

        self.cnx = sqlite3.connect(self.path)
        cur = self.cnx.cursor()
        for command in filtered_commands:
            cur.execute(command)
        cur.close()

    def generate_procid(self, set_id):
        cur = self.cnx.cursor()
        sql = "SELECT MAX(proc_id) FROM Components WHERE set_id={}".format(set_id)
        generator = cur.execute(sql)
        for i in generator:
            raw_id = i[0]
            if raw_id is None:
                proc_id = 1
            else:
                proc_id = int(raw_id) + 1
        cur.close()

        return proc_id

    def get_truespecs(self, conc):
        sql1 = "SELECT * FROM TrueSpecs WHERE tmao={}".format(conc)
        sql2 = "SELECT * FROM TrueSpecs WHERE tmao=99"
        spec1 = pd.read_sql(sql1, self.cnx)
        spec2 = pd.read_sql(sql2, self.cnx)
        return spec1, spec2

    def generate_metadata(self):
        cur = self.cnx.cursor()
        sql = "SELECT MAX(set_id) FROM Master"
        generator = cur.execute(sql)
        for i in generator:
            raw_id = i[0]
            if raw_id is None:
                set_id = 1
            else:
                set_id = int(raw_id) + 1
        cur.close()

        return set_id

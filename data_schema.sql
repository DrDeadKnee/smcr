PRAGMA foreign_keys = ON;

BEGIN TRANSACTION;

CREATE TABLE "RawData" (
    `rowid`	INTEGER	PRIMARY KEY,
    `set_id`	INTEGER NOT NULL,
    `filenumber`	INTEGER,
    `filename`  INTEGER,
    `signal`	INTEGER,
    `wavenumber`	INTEGER
);

CREATE TABLE "Master" (
    `rowid`	INTEGER PRIMARY KEY,
    `set_id`	INTEGER	NOT NULL,
    `proc_id`	INTEGER,
    `set_folder`	INTEGER,
    `tmao_conc`	INTEGER,
    `subtraction`	INTEGER,
    `file_count`	INTEGER,
    `anal_count`	INTEGER, 
    `normalization`	INTEGER,
    `qualguess`	INTEGER,
    `baseline_score` INTEGER,
    `class`	INTEGER,
    `comp_qual` INTEGER,
    `l1`	INTEGER,
    `l2`	INTEGER,
    `l3`	INTEGER,
    `l4`	INTEGER,
    `l5`	INTEGER,
    `noise`	INTEGER,
    `choice_1`	INTEGER,
    `choice_2`	INTEGER,
    `strong_in_weak`	INTEGER,
    `weak_in_strong`	INTEGER,
    `best_beta_m1`	INTEGER,
    `best_beta_m2`	INTEGER,
    `best_beta_m4`	INTEGER,
    `best_beta_m3`	INTEGER,
    `best_beta_m5`	INTEGER,
    `best_beta_cv`	INTEGER
);

CREATE TABLE "Components" (
    `rowid`	INTEGER PRIMARY KEY,
    `set_id`	INTEGER NOT NULL,
    `proc_id`   INTEGER,
    `r1_inner`	INTEGER,
    `r2_inner`	INTEGER, 
    `r1_outer`	INTEGER,
    `r2_outer`	INTEGER
);

CREATE TABLE "Solution" (
    `rowid`	INTEGER	PRIMARY KEY,
    `set_id`	INTEGER NOT NULL,
    `spec1_m1`	INTEGER,
    `spec2_m1`	INTEGER,
    `spec1_m2`	INTEGER,
    `spec2_m2`	INTEGER, 
    `spec1_m3`	INTEGER,
    `spec2_m3`	INTEGER,
    `spec1_m4`	INTEGER,
    `spec2_m4`	INTEGER,
    `spec1_m5`	INTEGER,
    `spec2_m5`	INTEGER,
    `wavenumber`	INTEGER
);

CREATE TABLE "CrossValidation" (
    `rowid`	INTEGER	PRIMARY KEY,
    `set_id`	INTEGER NOT NULL,
    `r1_inner`	INTEGER,
    `r2_inner`	INTEGER, 
    `r1_outer`	INTEGER,
    `r2_outer`	INTEGER,
    `blankspec`	INTEGER,
    `wavenumber`	INTEGER
);

CREATE TABLE "FileMaster" (
    `rowid`	INTEGER	PRIMARY KEY,
    `set_id`	INTEGER NOT NULL,
    `filename`	INTEGER,
    `filenumber`	INTEGER,
    `tmao`	INTEGER,
    `benzet`	INTEGER,
    `water`	INTEGER,
    `PCA1`	INTEGER,
    `PCA2`	INTEGER,
    `PCA3`	INTEGER
);

CREATE TABLE "AlignData"(
    `rowid`	INTEGER	PRIMARY KEY,
    `set_id`	INTEGER NOT NULL,
    `filenumber`	INTEGER,
    `filename`  INTEGER,
    `signal`	INTEGER,
    `wavenumber`	INTEGER
);

CREATE TABLE "TrueSpecs"(	
    `rowid`	INTEGER	PRIMARY KEY,
    `tmao`	INTEGER, 
    `signal`	INTEGER,
    `wavenumber`	INTEGER
);
CREATE INDEX set_id ON Master(set_id);
CREATE INDEX raw_set ON RawData(set_id);
CREATE INDEX comp_set ON Components(set_id);


COMMIT;

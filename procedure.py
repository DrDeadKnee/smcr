from factorize import Factorize
from matplotlib import pyplot as plt
import data_preprocessing
import configs
from curve_qc import QualityControl
import seaborn as sns
from ui import choose
from numpy import linspace
from matplotlib.pyplot import cm
import numpy as np


class Procedure():

    def __init__(self, dataframe):
        sns.set_style("ticks")
        self.raw_xdata, self.raw_ydata, self.names = self.parse_raw_dataframe(dataframe)
        self.findfactors = Factorize()
        self.comp_qual = None
        self.qualguess = None

    def run_proc(self):
        # clean it up
        self.xpretty, self.ypretty = data_preprocessing.clip_spectra(self.raw_xdata, self.raw_ydata)

        if configs.get("zero_curves"):
            self.xpretty, self.ypretty = data_preprocessing.vertical_offset(self.xpretty, self.ypretty)

        if configs.get("check_badspecs"):
            qc = QualityControl(self.xpretty, self.ypretty, self.names)
            qc.ask_for_approval()
            self.xpretty, self.ypretty = qc.get_data()

        elif not configs.get("check_badspecs"):
            print("WARNING: Manual Spectrum Selection is OFF")

        if configs.get("splinecorrect"):
            self.xpretty, self.ypretty = data_preprocessing.align_spectra(self.xpretty, self.ypretty)

        if configs.get("normalize"):
            self.ypretty = data_preprocessing.area_normalization(self.ypretty)

        plot_matrices(
            self.xpretty,
            self.ypretty,
            xax="wavenumebr (1/cm)",
            yax="intensity (a.u.)"
        )
        self.score_data(self.ypretty)

        # run smcr
        possible_comps, default_comps, default_loadings = self.findfactors.domethod("Lawton", self.ypretty)
        if configs.get("all_vis"):
            self.findfactors.plot_lawt()

        self.possible_components = possible_comps
        self.components = self.sort_components(possible_comps)
        self.fit_components(self.components)

        self.classify_results()
        self.show_final(self.components)

    def results_to_dict(self):
        results = {
            'subtraction': int(configs.get('zero_curves')),
            'baseline_score': self.baselinescore,
            'anal_count': len(self.xpretty),
            'normalization': int(configs.get('normalize')),
            'qualguess': self.qualguess,
            'class': self.guessclass,
            'comp_qual': self.comp_qual,
            'l1': self.findfactors.eigvals[-1],
            'l2': self.findfactors.eigvals[-2],
            'l3': self.findfactors.eigvals[-3],
            'l4': self.findfactors.eigvals[-4],
            'l5': self.findfactors.eigvals[-5],
            'noise': self.noise,
            'choice_1': self.choice_1,
            'choice_2': self.choice_2,
            'strong_in_weak': self.oneintwo,
            'weak_in_strong': self.twoinone
        }
        return results

    def classify_results(self):
        options = [
            "high_shoulders",
            "dipittybulge",
            "Barely_Altered_Water",
            "Altered_water",
            "Dangles_Only"
        ]
        question = "Which of these options best classifies results?"
        choice = choose(question, options)
        self.guessclass = options.index(choice) + 1

    def parse_raw_dataframe(self, dataframe):
        filenums = dataframe['filenumber'].unique()
        xmat = []
        ymat = []
        names = []
        for number in filenums:
            mask = dataframe['filenumber'].isin([number])
            xmat.append(dataframe['wavenumber'][mask].tolist())
            ymat.append(dataframe['signal'][mask].tolist())
            names.append(dataframe['filename'][mask].tolist()[0])

        return xmat, ymat, names

    def show_final(self, vectors):
        # normalize results
        renormalize = choose("Would you like to renormalize?", ["yes", "no"])
        if renormalize == "yes":
            component1 = vectors[0] / max(vectors[0])
            component2 = vectors[1] / max(vectors[1])

        elif renormalize == "no":
            component1 = vectors[0]
            component2 = vectors[1]

        components = [component1, component2]

        # display results
        plt.clf()
        if configs.get('all_vis'):
            plt.ioff()

        sns.set_context("paper",  font_scale=1.5)
        plt.xlabel("wavenumber (1/cm)")
        plt.ylabel("intensity (a.u.)")

        for i in range(len(components)):
            name = "component {}".format(i + 1)
            plt.plot(self.xpretty[i], components[i], label=name)
        plt.legend(loc="best")
        plt.show()

        if configs.get('qual_eval'):
            question = "What is the quality of your categorization?"
            options = [1, 2, 3, 4, 5]
            self.comp_qual = choose(question, options)

    def sort_components(self, vectors):

        # show second two possible component spectra
        plt.clf()
        plt.ion()
        sns.set_context("paper",  font_scale=1.5)
        plt.xlabel("wavenumber (1/cm)")
        plt.ylabel("intensity (a.u.)")

        plt.plot(vectors[1], color="blue", label="Inner bound for region 1")
        plt.plot(vectors[2], color="green", label="Inner bound for region 2")
        plt.legend(loc="best")
        plt.show()

        question = "Which of these spectra most likely represents a component?"
        choices = ["region 1 inner - blue", "region 2 inner - green"]
        inner_choice = choose(question, choices)

        # show first two possible component spectra
        plt.clf()

        sns.set_context("paper",  font_scale=1.5)
        plt.xlabel("wavenumber (1/cm)")
        plt.ylabel("intensity (a.u.)")

        plt.plot(vectors[0], color="blue", label="Outer bounds for region 1")
        plt.plot(vectors[3], color="green", label="Outer bounds for region 2")
        plt.legend(loc="best")
        plt.show()

        question = "Which of these spectra most likely represents a component?"
        choices = ["region 1 outer - blue", "region 2 outer - green"]
        outer_choice = choose(question, choices)

        choicedict = {
            "region 1 outer - blue": 0,
            "region 2 outer - green": 3,
            "region 1 inner - blue": 1,
            "region 2 inner - green": 2
        }

        if choicedict[inner_choice] % 2 != choicedict[outer_choice] % 2:
            print("Those two choices don't match. Try again.")
            guesses = self.sort_components(vectors)
        else:
            guesses = [
                vectors[choicedict[inner_choice]],
                vectors[choicedict[outer_choice]]
            ]

        self.choice_1 = outer_choice
        self.choice_2 = inner_choice
        return guesses

    def fit_components(self, components):
        weight1 = []
        weight2 = []

        for i in range(len(self.ypretty)):
            y = np.asarray(self.ypretty[i])
            c1 = components[0]
            c2 = components[1]

            '''w2 = (
                (np.sum(c2 * y) / np.sum(c2 * c2)
                - (np.sum(c1 * y) * np.sum(c2 * c1)) / (np.sum(c2 * c2) * np.sum(c1 * c1))) /
                (1 - (np.sum(c2 * c1)**2) / (np.sum(c2 * c2) * np.sum(c2 * c2)))
            )
            w1 = (np.sum(c1 * y) - w2 * np.sum(c2 * c1)) / np.sum(c2 * c2)'''

            X = np.matrix([c1, c2])
            W = np.dot(X, y) * np.linalg.inv(np.dot(X, X.transpose()))
            W = W.tolist()
            w1, w2 = W[0][0], W[0][1]

            weight1.append(w1)
            weight2.append(w2)

        sqerr = 0
        for i in range(len(weight1)):
            sqerr += (self.ypretty[i] - (weight1[i] * components[0] + weight2[i] * components[1]))**2
        self.noise = np.mean(sqerr)
        self.oneintwo = weight1[-1]
        self.twoinone = weight2[0]

    def score_data(self, ymat):
        # estimate baseline variance
        bl = []
        for i in ymat:
            bl.append(np.mean(i[0:20]))
        self.baselinescore = np.mean(bl)

        if configs.get("qual_eval"):
            choice = choose("How good do you think the original data are?", options=[1, 2, 3, 4, 5])
            self.qualguess = choice


def plot_matrices(xmat, ymat, xax=None, yax=None, names=None):
    plt.clf()
    sns.set_context("paper",  font_scale=1.5)
    color = iter(cm.rainbow(linspace(0, 1, len(ymat))))
    if xax is not None:
        plt.xlabel(xax)

    if yax is not None:
        plt.ylabel(yax)

    if names is None:
        for i in range(len(xmat)):
            plt.plot(xmat[i], ymat[i])

    elif names is not None:
        for i in range(len(xmat)):
            name = names[i].replace(".txt", "")
            plt.plot(xmat[i], ymat[i], label=name, c=next(color))
        plt.legend(loc="best")

    plt.show()

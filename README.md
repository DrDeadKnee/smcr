Software to run self modelling curve resolution
SMCR = self modelling curve reolution
see the paper:
        William H. Lawton & Edward A. Sylvestre (1971),
        Self Modeling Curve Resolution, Technometrics, 13:3, 617-633


the TLDR frm the paper:

	step 1: do PCA
	step 2: find a space of all rotations possible for the first two eignvectors of the covariance
		matrix of the dataset, assuming that all elements of the true data are positive
	step 3: choose a single point in the plane associated with the weights of these two eigenvectors
		wich accurately represents the seperate spectra of the underlying species

I am currently working on step 3. Which isn't really adressed in the paper.
The nimfa libraries are nonessential and can probably be removed
I have no Idea how you could possibly do this without matplotlib

pip install -r requirements.txt

# curve quality control module
from ui import choose
from matplotlib import pyplot as plt
from matplotlib.pyplot import cm
from numpy import linspace
from pylab import get_current_fig_manager
from matplotlib import use


class QualityControl():

    def __init__(self, xmat, ymat, curvenames, norm_method="highpeak", xtoreference=3060):
        self.xtogive, self.ytogive = xmat, ymat
        self.xmat = xmat
        self.ymat = ymat
        self.names = curvenames
        self.xref = xtoreference
        self.prep_data(norm_method)
        self.plot_for_inspection()

    def prep_data(self, norm_method):
        # prepares two matrices: one normalized to the highest peak, and one reduced by the
        # "blank" spectrum
        # create the normalized matrix
        self.normalmat = []
        if norm_method == "highpeak":
            for i in range(len(self.ymat)):
                maxval = float(max(self.ymat[i]))
                maxvec = []
                for j in range(len(self.ymat[i])):
                    maxvec.append(self.ymat[i][j] / maxval)
                self.normalmat.append(maxvec)
        elif norm_method == "noiserat":
            for i in range(len(self.ymat)):
                maxval = float(max(self.ymat[i]))
                minval = float(min(self.ymat[i]))
                normvec = []
                for j in range(len(self.ymat[i])):
                    normvec.append((self.ymat[i][j] - minval) / (maxval))
                self.normalmat.append(normvec)

        # find the blank spectrum
        # assumes the blank spectrum has the lowest value at the signal peak
        # finds the nearest int in a list for the first spectrum, assuming reasonable alignment
        nearest_xindex = 0
        smallest_difference = max([self.xmat[0][0], self.xmat[0][-1]])
        for i in range(len(self.xmat[0])):
            difference = ((self.xmat[0][i] - self.xref)**2)**0.5
            if difference <= smallest_difference:
                smallest_difference = difference
                nearest_xindex = i

        # find the curve with the lowest value at the specified index
        reference_values = []
        for i in range(len(self.ymat)):
            reference_values.append(self.normalmat[i][nearest_xindex])

        reference_curve_index = reference_values.index(min(reference_values))

        print("\n  Found that %s is the 'blank' spectrum, assuming signal at %s" % (self.names[reference_curve_index], self.xref))
        self.rc_index = reference_curve_index

        # create a matrix
        self.differencemat = []
        for i in range(len(self.normalmat)):
            spectral_difference = []
            for j in range(len(self.normalmat[i])):
                spectral_difference.append(self.normalmat[i][j] - self.normalmat[reference_curve_index][j])
            self.differencemat.append(spectral_difference)

    def plot_for_inspection(self):
        plt.clf()
        plt.ion()
        use("wx")
        plt.figure(1, figsize=(9.5, 9.5))
        plt.xlabel("Wavenumber (1/cm)")
        plt.subplot(211)
        plt.ylabel("Normalized Intensity")
        color = iter(cm.rainbow(linspace(0, 1, len(self.ymat))))
        for i in range(len(self.normalmat)):
            plt.plot(self.xmat[i], self.normalmat[i], label=self.names[i], c=next(color))
        plt.legend(loc="best", prop={"size": 10})

        plt.subplot(212)
        plt.ylabel("Difference in Normalized Intensity")

        color = iter(cm.rainbow(linspace(0, 1, len(self.ymat))))
        for i in range(len(self.differencemat)):
            plt.plot(self.xmat[i], self.differencemat[i], c=next(color))

        thisman = get_current_fig_manager()
        thisman.window.wm_geometry("+0+0")
        plt.show()

    def ask_for_approval(self):

        question = "Do you think some of these spectra do not belong in this dataset?"
        options = ["yes", "no"]
        answer = choose(question, options)

        if answer == "yes":
            question = "What curve would you like to remove?"
            options = self.names
            answer = choose(question, options)
            self.remove_selection(answer)
            plt.clf()
            self.plot_for_inspection()
            self.ask_for_approval()

        elif answer == "no":
            pass

    def ask_for_blank(self):
        question = "Does the automatic pick correspond to a blank?"
        options = ["yes", "no"]
        answer = choose(question, options)
        nocurvetoggle = False

        if answer == "no":
            question = "Which of the following DOES?"
            options = self.names
            options.append("None of the above")
            answer = choose(question, options)

            if answer == "None of the above":
                nocurvetoggle = True
            else:
                idx = self.names.index(answer)
                curvename = answer

        elif answer == "yes":
            idx = self.rc_index
            curvename = self.names[idx]

        if not nocurvetoggle:
            self.blank = self.ymat[idx]
            self.remove_selection(curvename)

        else:
            self.blank = "None"

    def remove_selection(self, curvename):
        idx = self.names.index(curvename)
        del self.xmat[idx]
        del self.ymat[idx]
        del self.names[idx]
        del self.normalmat[idx]
        del self.differencemat[idx]

    def get_data(self):
        return self.xtogive, self.ytogive

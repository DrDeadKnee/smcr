'''
ui enabled expolaration of a single dataset through self modelling curve resolution

Usage:
    main.py <holder_folder> <database>

Options:
    -h --help       Print this usage.
'''

from docopt import docopt
import os
from ui import choose, ask
from procedure import Procedure
from data_handler import DataHandler
from betamax import BetaMax
from cv_beta import CVBeta


class Main(object):

    def __init__(self, database, holder_folder):
        self.database = database
        self.holder_folder = holder_folder
        self.dbh = DataHandler(database)
        self.conc = None
        self.main_menu()

    def main_menu(self):
        question = "What do you want to do?"
        options = [
            "Add data to database",
            "Analyze Raw data",
            "Change current folder",
            "Change working concentration",
            "Cross-validate for beta",
            "Use raw data_DKL",
            "Use twospecs = f(y1, y2, c1, c2) for beta",
            "Use basic distance = f(y1 - c1) for beta",
            "Evaluate set by 5 metrics",
            "Remove Dataset"
        ]

        while True:
            folderstring = ""
            for i in self.get_folders(self.holder_folder):
                folderstring += "\t" + i + "\n"

            print("\nCurrent concentration is {}".format(self.conc))
            print("Currently working in {}".format(self.holder_folder))
            print("Contained Folders:\n {}".format(folderstring))

            choice = choose(question, options)
            if choice == "Add data to database":
                self.dump_datafolder()
            elif choice == "Analyze Raw data":
                self.process_Raw()
            elif choice == "Change current folder":
                self.change_folder()
            elif choice == "Change working concentration":
                self.set_concentration()
            elif choice == "Cross-validate for beta":
                self.find_best_beta("cross_validataion")
            elif choice == "Use raw data_DKL":
                self.find_best_beta("fixwat")
            elif choice == "Use twospecs = f(y1, y2, c1, c2) for beta":
                self.find_best_beta("truespec2")
            elif choice == "Use basic distance = f(y1 - c1) for beta":
                self.find_best_beta("basic")
            elif choice == "Evaluate set by 5 metrics":
                self.fivemetbetas()
            elif choice == "Remove Dataset":
                self.remove_dataset()

    def dump_datafolder(self):
        if self.conc is None:
            self.set_concentration()
        folders = self.get_folders(self.holder_folder)
        folders.append("Cancel")
        question = "Which folder to dump?"
        choice = choose(question, folders)
        if choice == "Cancel":
            pass
        else:
            folder = os.path.join(self.holder_folder, choice)
            self.dbh.dump_to_db(folder, self.conc)
            self.dbh.update_filemaster(folder)

    def fivemetbetas(self):
        if self.conc is None:
            self.set_concentration()

        master = self.dbh.get_master()
        mask = master['tmao_conc'] == self.conc
        options = master['set_folder'][mask].tolist()
        options.append("Cancel")
        choice = choose("Which folder to analyze?", options)

        if choice == "Cancel":
            pass

        else:
            dataset_id = master['set_id'][master['set_folder'].isin([choice])].tolist()[0]
            data = self.dbh.select_dataset("AlignData", dataset_id)
            if len(data.index) < 1000:
                print("That dataset is not aligned.")

            cvb = CVBeta(data)
            cvb.vis = False

            print("Implementing method 1...")
            cvb.run_truespec_regression(self.conc, self.dbh, compare=0)

            print("Implementing method 2...")
            cvb.one_hunred_betas(cvb.components, "truespec1")

            print("Implementing method 4...")
            cvb.one_hunred_betas(cvb.components, "truespec2")

            cvb.altmethod = True
            print("Implementing method 5...")
            cvb.one_hunred_betas(cvb.components, "truespec1")

            print("Implementing method 3...")
            cvb.one_hunred_betas(cvb.components, "truespec2")
            self.write_betas(dataset_id, cvb.allresults)

            print("Implementing method 6...")
            cvb.one_hunred_betas(cvb.components, 'truespec1')

    def write_betas(self, dataset_id, results):

        res_to_master = {
            "set_id": dataset_id,
        }

        res_to_soln = {
            "set_id": dataset_id,
            'wavenumber': results['wavenumber'],
        }

        for i in results:
            if 'best' in i:
                res_to_master[i] = results[i]
            if 'spec' in i:
                res_to_soln[i] = results[i]

        self.dbh.update_master(dataset_id, res_to_master)
        self.dbh.write_generic_results("Solution", res_to_soln)

    # TODO: Refactor (see process Raw)
    def find_best_beta(self, method):
        if self.conc is None:
            self.set_concentration()

        master = self.dbh.get_master()
        mask = master['tmao_conc'] == self.conc
        options = master['set_folder'][mask].tolist()
        options.append("Cancel")
        choice = choose("Which folder to analyze?", options)

        if choice == "Cancel":
            pass

        else:
            dataset_id = master['set_id'][master['set_folder'].isin([choice])].tolist()[0]
            data = self.dbh.select_dataset("AlignData", dataset_id)
            if len(data.index) < 1000:
                print("That dataset is not aligned.")
            else:
                cvb = CVBeta(data)
                if method == "cross_validation":
                    cvb.run_cross_validation()
                elif method == "truespec1":
                    cvb.run_truespec_regression(self.conc, self.dbh)
                elif method == "truespec2":
                    cvb.run_truespec_regression(self.conc, self.dbh, compare=2)
                elif method == "basic":
                    cvb.run_truespec_regression(self.conc, self.dbh, compare=0)
                elif method == 'fixwat':
                    cvb.run_truespec_regression(self.conc, self.dbh, compare=3)
                results = cvb.results

                methodex = [
                    [],
                    ['best_beta_m1', 'spec1_m1', 'spec2_m1'],
                    ['best_beta_m2', 'spec1_m2', 'spec2_m2'],
                    ['best_beta_m3', 'spec1_m3', 'spec2_m3'],
                    ['best_beta_m4', 'spec1_m4', 'spec2_m4'],
                    ['best_beta_m5', 'spec1_m5', 'spec2_m5'],
                ]

                res_to_master = {
                    "set_id": dataset_id,
                    "{}".format(methodex[results['method']][0]): results['beta']
                }

                res_to_soln = {
                    "set_id": dataset_id,
                    'wavenumber': results['wavenumber'],
                    "{}".format(methodex[results['method']][1]): results['c1'],
                    "{}".format(methodex[results['method']][2]): results['c2']
                }

                self.dbh.update_master(dataset_id, res_to_master)
                self.dbh.write_generic_results("Solution", res_to_soln)

    # TODO: Refactor (see cross-validation)
    def process_Raw(self):
        if self.conc is None:
            self.set_concentration()

        # get data
        master = self.dbh.get_master()
        mask = master['tmao_conc'] == self.conc
        unanalyzed = master['proc_id'].isnull()
        print("\nThe following datasets have yet to be analyzed:")
        unannames = master['set_folder'][mask & unanalyzed].tolist()
        for i in unannames:
            print(i)

        options = master['set_folder'][mask].tolist()
        options.append("Cancel")
        choice = choose("Which folder to analyze?", options)
        if choice == "Cancel":
            pass

        else:
            dataset_id = master['set_id'][master['set_folder'].isin([choice])].tolist()[0]
            data = self.dbh.select_dataset("RawData", dataset_id)

            # run procedure
            proc = Procedure(data)
            proc.run_proc()
            results = proc.results_to_dict()
            solution_vectors = proc.possible_components
            wavenumbers = proc.xpretty[0]

            # write components
            self.dbh.write_vectors(dataset_id, solution_vectors)

            # get best beta values
            bm = BetaMax(self.database, dataset_id)
            c1_m3, c2_m3, F_m3, bestbet_m3 = bm.one_hunred_betas(method=3)
            c1_m5, c2_m5, F_m5, bestbet_m5 = bm.one_hunred_betas(method=5)

            results["best_beta_m3"] = bestbet_m3
            results["best_beta_m5"] = bestbet_m5

            # write updated results
            beta_results = {
                "set_id": dataset_id,
                "spec1_m3": c1_m3,
                "spec2_m3": c2_m3,
                "spec1_m5": c1_m5,
                "spec2_m5": c2_m5,
                "wavenumber": wavenumbers
            }
            self.dbh.update_master(dataset_id, results)
            self.dbh.write_generic_results("Solution", beta_results)

    def change_folder(self):
        folders = self.get_folders(self.holder_folder)
        folders.append("Up One Level")
        folders.append("Stay Here")
        choice = choose("Where do you want to go?", folders)

        if choice == folders[-2]:
            last_entry = self.holder_folder.split("/")[-1]
            self.holder_folder = self.holder_folder[:-(len(last_entry) + 1)]
        elif choice == folders[-1]:
            pass
        else:
            newfolder = os.path.join(self.holder_folder, choice)
            if len(self.get_folders(newfolder)) < 1:
                print("That folder doesn't contain any subfolders... Ignoring choice.")
            else:
                self.holder_folder = newfolder

        print("Now working in {}".format(self.holder_folder))

    def get_folders(self, holder_folder):
        contents = os.listdir(holder_folder)
        folders = []
        for item in contents:
            path = os.path.join(holder_folder, item)
            if os.path.isdir(path):
                folders.append(item)
        return folders

    def remove_dataset(self):
        master = self.dbh.get_master()
        options = master['set_folder'][master['tmao_conc'].isin([self.conc])].tolist()
        options.append("Cancel")
        choice = choose("Which set to remove?", options)
        if choice != "Cancel":
            dataset_id = master['set_id'][master['set_folder'].isin([choice])].tolist()[0]
            self.dbh.delete_dataset(dataset_id)
        else:
            pass

    def set_concentration(self):
        print("Current concentration is {} M".format(self.conc))
        question = "What concentration do you want to work in?"
        self.conc = ask(question, float())


if __name__ == "__main__":
    args = docopt(__doc__)
    database = args['<database>']
    holder_folder = args['<holder_folder>']

    Main(database, holder_folder)
